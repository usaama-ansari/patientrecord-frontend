import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//components
import { HomeComponent } from './pages/home/home.component';
import { AddComponent } from './pages/add/add.component';
import { ModifyComponent } from './pages/modify/modify.component';
import { ViewComponent } from './pages/view/view.component';
import { SettingsComponent } from './pages/settings/settings.component';

const routes: Routes = [
    {path:'', component : HomeComponent },
    {path:'add', component : AddComponent },
    {path:'modify', component : ModifyComponent },
    {path:'view', component : ViewComponent },
    {path:'settings', component : SettingsComponent },
]

@NgModule({
    imports:[
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
})

export  class AppRoutingModule {

}