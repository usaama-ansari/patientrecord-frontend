import { Component, OnInit, OnDestroy } from '@angular/core';
import { ElectronService } from 'ngx-electron';
import { PatientService } from '../services/patient.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  current_menu_subs: Subscription;
  currentMenu: string='';
  constructor(
    private _electronService: ElectronService,
    private _patientService: PatientService
  ) { }

  ngOnInit() {
    this.current_menu_subs = this._patientService.current_menu_subject.subscribe((menuName) => {
      this.currentMenu = menuName;
    });
    this.currentMenu = 'Menu';
  }

  ngOnDestroy() {
    this.current_menu_subs.unsubscribe();
  }

  changeToHome() {
    this.currentMenu = 'Menu';
  }

  close() {
    this._electronService.ipcRenderer.send('window:close');
  }
  max() {
    this._electronService.ipcRenderer.send('window:max');
  }
  min() {
    this._electronService.ipcRenderer.send('window:min');
  }



}
