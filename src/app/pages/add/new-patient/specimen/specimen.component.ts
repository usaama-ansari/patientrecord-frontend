import {
  Component,
  OnInit, Input, Output, EventEmitter, OnDestroy
} from '@angular/core';
import { FormControl, FormGroup, FormArray, Validators } from '@angular/forms';
import { InvestigationInterface, NewTestInterface } from '../../../../models/patient-record.model';
import { SuggesstionsService } from '../../../../services/suggesstions.service';
import { Subscription } from 'rxjs/Subscription';
import * as _ from 'lodash';
import * as moment from 'moment';
import {
  investigationParameters,
  investigationResults,
  clinicalDiagnosis,
  specimensArray
} from '../../../../config/config';
@Component({
  selector: 'app-specimen',
  templateUrl: './specimen.component.html',
  styleUrls: ['./specimen.component.css']
})
export class SpecimenComponent implements OnInit, OnDestroy {
  @Output() test = new EventEmitter<NewTestInterface>();
  @Output() dismissTestForm = new EventEmitter<any>();
  suggObject: { source: string, suggArray: string[] } = { source: '', suggArray: [] };
  testRecord: FormGroup;
  todays_date: string;
  clinicalDiagnosis = clinicalDiagnosis;
  // array to iterate select options
  investigationParameters = investigationParameters;
  // array to iterate select options  
  investigationResults = investigationResults;
  specimensArray = specimensArray;
  today: string;
  //Array to be populated by user
  investigationsAdded: InvestigationInterface[] = [];
  sugg_rec_sub_subs: Subscription;

  constructor(
    private _sugService: SuggesstionsService,
  ) {
    this.today = moment().format('YYYY-MM-DD');
  }

  ngOnInit() {
    this.initializeForm();
    this.sugg_rec_sub_subs = this._sugService.sugg_rec_subject.subscribe(
      (suggesstions: { source: string, suggArray: string[] }) => {
        this.suggObject = suggesstions;
      });
  }

  ngOnDestroy() {
    this.sugg_rec_sub_subs.unsubscribe();
  }

  onDateSelected() {
    console.log('date selected')
  }
  saveTest() {
    if (this.testRecord.valid) {
      var submitableForm = _.cloneDeep(this.testRecord.value);
      delete submitableForm.investigation;
      submitableForm['investigations'] = this.investigationsAdded;
      //ALSO REINITIALIZE THE FORM
      this.initializeForm();
      this.investigationsAdded = [];
      this.test.emit(submitableForm);
    } else {
      alert('Please fill all mandatory fields.')
    }
  }

  cancelSaveTest() {
    this.dismissTestForm.emit();
  }


  addInvestigation(investigation) {
    this.investigationsAdded.push(investigation);
  }
  removeInvestigation(index: number) {
    this.investigationsAdded.splice(index, 1);
  }
  getSuggestions(source: string, value: string) {
    this._sugService.get_suggesstions(source, value);
  }

  resetForm() {

  }
  initializeForm() {
    this.testRecord = new FormGroup({
      'labNo': new FormControl(null, Validators.required),
      'dateOfCollection': new FormControl(this.today, Validators.required),
      'dateOfTesting': new FormControl(''),
      'specimen': new FormControl(null, Validators.required),
      'testName': new FormControl(null, Validators.required),
      'serumValue': new FormControl(null),
      'physician': new FormControl(null, Validators.required),
      'clinicalDiagnosis': new FormControl(null, Validators.required),
      'investigation': new FormGroup({
        'parameter': new FormControl(null, Validators.required),
        'result': new FormControl(null)
      }),
    })
  }
}
