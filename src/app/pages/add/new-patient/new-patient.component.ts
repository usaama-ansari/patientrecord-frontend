import { Component, OnInit, OnDestroy, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { FormControl, FormGroup, FormArray, Validators } from '@angular/forms';
import { Router } from '@angular/router';
//### Importing patient data Interface
import {
  Patient,
  PatientRecordInterface,
  TestInterface,
  NewTestInterface,
  InvestigationInterface
} from '../../../models/patient-record.model';
import { PatientService } from '../../../services/patient.service';
import { MatSnackBar } from '@angular/material';
import { ElectronService } from 'ngx-electron';
import { Subscription } from 'rxjs/Subscription';


@Component({
  selector: 'app-new-patient',
  templateUrl: './new-patient.component.html',
  styleUrls: ['./new-patient.component.css']
})
export class NewPatientComponent implements OnInit, OnDestroy {
  @ViewChild('submitBtn') submitBtn: ElementRef;
  patientRecord: PatientRecordInterface;
  newPatientId: string = null;
  ptnt_id_subj_subs: Subscription;
  investigationsAdded: InvestigationInterface[] = [];
  patientInfo: FormGroup;
  tests: NewTestInterface[] = [];
  showSpecimenComponent: boolean = false;
  constructor(
    private _patientService: PatientService,
    private _electron: ElectronService,
    private _renderer: Renderer2,
    private _router: Router,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    //### INITIALIZING THE FORM USING REACTIVE APPROACH
    //============================================
    this.initializeForm();
    //initialize tests array to prevent push error
    this.subscribe_patient_id_subject();
    //============================================
  }

  ngOnDestroy() {
    this.ptnt_id_subj_subs.unsubscribe();
  }

  resetForm() {
    this.initializeForm();
  }
  //To restructure the form filled by appending different parts like investigation array
  //Direct form created through 'FormGroup' is not submitted

  addSpecimen() {
    //save patient info
    if (this.patientInfo.valid) {
      this.patientRecord = this.patientInfo.value;
      this.showSpecimenComponent = true;
    } else {
      alert('Please fill all fields.')
    }
  }

  deleteAddedTest(index: number) {
    this.tests.splice(index, 1);
  }

  onTestFormDismiss() {
    this.showSpecimenComponent = false;
  }

  onTestAdded(test: NewTestInterface) {
    this.tests.push(test);
    this.patientRecord.tests = this.tests;
    this.showSpecimenComponent = false;
  }

  prepareBeforeSubmit() {

  }

  // SUBMIT FOR TO BACKEND AFTER RESTRUCTURING THE SUBMITABLE FORM
  submitForm() {
    this.patientRecord.patientId = this.newPatientId;
    this._patientService.addNewPatient(this.patientRecord);
    alert('Patient record added!');
    this._router.navigate(['/']);
    //receive response    
    this._electron.ipcRenderer.on('patient:created', (event, status) => {

    });
  }

  addInvestigation(investigation: InvestigationInterface) {
    this.investigationsAdded.push(investigation);
  }
  removeInvestigation(index: number) {
    this.investigationsAdded.splice(index, 1);
  }


  initializeForm() {
    //fetch new Patient id 
    this._patientService.getId();
    this.patientInfo = new FormGroup({
      'name': new FormControl(null, Validators.required),
      'age': new FormControl(null, Validators.required),
      'gender': new FormControl('male'),
      'altId': new FormControl(null),
    });
  }

  // subject returned in response to getId call in initializeForm() above
  subscribe_patient_id_subject() {
    this.ptnt_id_subj_subs = this._patientService.patient_id_subject.subscribe((response: any) => {
      var onePlusCounter;
      if (response['success']) {
        onePlusCounter = response['msg'].patient_id + 1;
      } else {
        onePlusCounter = 1;// only in case if app is run very first time
      }
      var year = new Date().getFullYear().toString();
      var yearToArray = year.split('');
      yearToArray.splice(0, 2);
      var yearToString = yearToArray.join("");
      this.newPatientId = 'VRDL' + yearToString + '-' + onePlusCounter;
    });
  }



}// END OF CLASS



