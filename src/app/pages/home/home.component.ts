import { Component, OnInit } from '@angular/core';
import { PatientService } from '../../services/patient.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private _patientService: PatientService
  ) { }

  ngOnInit() {
    this.changeCurrentMenu('')
  }
  changeCurrentMenu(menuName: string) {
    this._patientService.current_menu_subject.next(menuName);
  }

}
