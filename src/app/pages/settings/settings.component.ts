import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PatientService } from '../../services/patient.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  @ViewChild('from') from: ElementRef;
  @ViewChild('to') to: ElementRef;
  set_id: number = null;
  year: string = '';
  constructor(
    private _patientService: PatientService
  ) { }

  ngOnInit() {
    this.year = String(new Date().getUTCFullYear()).substring(2, 4);
  }



  doBackup(mode: number) {
    if (mode === 0 && this.from.nativeElement.value && this.to.nativeElement.value) {
      let timeRange = { from: this.from.nativeElement.value, to: this.to.nativeElement.value };
      this._patientService.backUpRecords(timeRange);
    } else {
      this._patientService.backUpRecords(null);
    }
  }

  setId() {
    if (this.set_id == null || this.set_id == undefined) {
      alert('Id can not be blank')
    } else if(this.set_id<=0){
      alert('Id must be greater than 0')
    }else{
      this._patientService.setId(this.set_id);
    }
  }


}



