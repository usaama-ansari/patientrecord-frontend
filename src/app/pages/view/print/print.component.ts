import { Component, OnInit, Input, ElementRef, Renderer2 } from '@angular/core';
import { PatientRecordInterface, NewTestInterface, InvestigationInterface } from '../../../models/patient-record.model';
import { ViewService } from '../view.service';
import { PatientService } from '../../../services/patient.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-print',
  templateUrl: './print.component.html',
  styleUrls: ['./print.component.css']
})
export class PrintComponent implements OnInit {
  @Input('patientToPrint') patientToPrint: PatientRecordInterface;
  testToPrint: NewTestInterface = null;
  activeTest: NewTestInterface = null;
  testArrayToPrint: NewTestInterface[] = [];
  testCollectionToPrint = {};
  investigationsToPrint: InvestigationInterface[] = [];
  printableRecord: any = null;
  selected: number = null;
  constructor(
    private _renderer: Renderer2,
    private _elementRef: ElementRef,
    private _viewService: ViewService,
    private _patientService: PatientService
  ) { }

  ngOnInit() {
    this.onTestSelect(0);//initialize with first item selected
    this._patientService.current_menu_subject.next('Print Record');
  }

  onTestSelect(index: number) {
    // check if entry is there on selected index in testArrayToPrint
    if (this.testCollectionToPrint[index] === undefined || this.testCollectionToPrint[index] === null) {
      //add checked key in investigation object
      var cloned_test = _.cloneDeep(this.patientToPrint.tests[index]);
      //modded_investigation is with additional 'checked': true/false key value
      var modded_investigations = cloned_test.investigations.map(function (investigation) {
        investigation['checked'] = false;
        return investigation;
      });
      cloned_test.investigations = modded_investigations;
      this.activeTest = cloned_test;
    } else {
      this.activeTest = _.cloneDeep(this.testCollectionToPrint[index]);
    }
    this.selected = index;
  }





  isActive(index: number) {
    return this.selected === index;
  }


  onInvestigationSelect(isChecked: boolean, index: number) {
    if (isChecked) {
      this.activeTest.investigations[index]['checked'] = true;
      this.testCollectionToPrint[this.selected] = this.activeTest;
    } else {
      this.activeTest.investigations[index]['checked'] = false;
      let all_unchecked: boolean = true;
      this.activeTest.investigations.forEach(function (investigation) {
        if (investigation['checked']) {
          all_unchecked = false;
        }
      });
      if (all_unchecked) {
        delete this.testCollectionToPrint[this.selected]
      }
    }
  }

  checkTable() {

  }

  onPrint() {
    let tctp = this.testCollectionToPrint;
    this.testArrayToPrint = Object.keys(tctp).map(function(keyindex){
      return tctp[keyindex];
    });
  
    if (this.testArrayToPrint.length === 0) {
      alert('Please select at least one investigation to print.')
    } else {
      if (this.testArrayToPrint.length > 1) {
        // ON MULTI PRINT
        var trimmed_test_array = [];
        this.testArrayToPrint.forEach(function (test) {
          var cloned_test = _.cloneDeep(test);
          var unchecked_trimmed_investigation = cloned_test.investigations.filter(function (investigation) {
            return investigation['checked'];
          });
          cloned_test.investigations = unchecked_trimmed_investigation
          trimmed_test_array.push(cloned_test);
        });
        this.printableRecord = {
          name: this.patientToPrint.name,
          age: this.patientToPrint.age,
          altId: this.patientToPrint.altId,
          gender: this.patientToPrint.gender,
          labNo: this.patientToPrint.labNo,
          patientId: this.patientToPrint.patientId,
          testArrayToPrint: trimmed_test_array
        }
        this._viewService.printMultiReport(this.printableRecord);
        this.printableRecord = null;
        this.onTestSelect(0);
      } else {
        // ON SINGLE PRINT
        var unchecked_trimmed_investigation = this.testArrayToPrint[0].investigations.filter(function (investigation) {
          return investigation['checked'];
        });
        var cloned_test = _.cloneDeep(this.testArrayToPrint[0]);
        cloned_test.investigations = unchecked_trimmed_investigation;
        this.printableRecord = {
          name: this.patientToPrint.name,
          age: this.patientToPrint.age,
          altId: this.patientToPrint.altId,
          gender: this.patientToPrint.gender,
          labNo: this.patientToPrint.labNo,
          patientId: this.patientToPrint.patientId,
          testToPrint: cloned_test
        }
        this._viewService.printReport(this.printableRecord);
        this.printableRecord = null;
        this.onTestSelect(0);
      }
    }
  }






}

