import { Component, OnInit, OnDestroy } from '@angular/core';
import { PatientRecordInterface } from '../../models/patient-record.model';
import { PatientService } from '../../services/patient.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { ViewService } from './view.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/take';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css'],
  providers: [ViewService]
})
export class ViewComponent implements OnInit, OnDestroy {
  patientsArray: PatientRecordInterface[];
  patient_list_subject_subscription: Subscription;
  patientToPrint: PatientRecordInterface = null;
  patientToView: PatientRecordInterface = null;
  viewOrPrint: boolean = false;
  showPrintPage: boolean = false;
  showViewPage: boolean = false;
  constructor(
    private _patientService: PatientService
  ) { }

  ngOnInit() {
    this.patient_list_subject_subscription = this._patientService.patient_list_subject.subscribe((response) => {
      this.patientsArray = [];
      this.patientsArray = response['msg'];
    });
    this._patientService.fetchPatients({ search_by: null, value: null });
  }

  ngOnDestroy() {
    this.patient_list_subject_subscription.unsubscribe();
  }

  onPrintStart(patient: PatientRecordInterface, index: number) {
    this.patientToPrint = patient;
    this.viewOrPrint = true;
    this.showPrintPage = true;
  }

  onViewStart(patient: PatientRecordInterface, index: number) {
    this.patientToView = patient;
    this.viewOrPrint = true;
    this.showViewPage = true;
  }

  searchPatient(search_by: string, value: string) {
    this._patientService.fetchPatients({ search_by, value });
  }

  goBack() {
    this.showPrintPage = false;
    this.showViewPage = false;
    this.viewOrPrint = false;
    this._patientService.current_menu_subject.next('View / Print Records')
  }
}
