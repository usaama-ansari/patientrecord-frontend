import { Component, OnInit, Input } from '@angular/core';
import { PatientService } from '../../../services/patient.service';
import { PatientRecordInterface, NewTestInterface} from '../../../models/patient-record.model';
@Component({
  selector: 'app-full-view',
  templateUrl: './full-view.component.html',
  styleUrls: ['./full-view.component.css']
})
export class FullViewComponent implements OnInit {
  @Input() patientToView: PatientRecordInterface;
  testToView: NewTestInterface;
  selected: number = 0;
  constructor(
    private _patientService: PatientService
  ) { }

  ngOnInit() {
    this._patientService.current_menu_subject.next('View Record');
    this.onSelect(0);
  }

  onSelect(testIndex: number) {
    this.testToView = this.patientToView.tests[testIndex];
    this.selected = testIndex;
  }

  isActive(index: number) {
    return this.selected === index;
  }
}
