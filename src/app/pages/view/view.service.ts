import { Injectable, NgZone } from '@angular/core';
import { ElectronService } from 'ngx-electron';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ViewService {

  constructor(
    private _electronService: ElectronService,
    private zone: NgZone
  ) { }


  init_ipc() {
    this._electronService.ipcRenderer.on('patient:onPrintComplete', (event, response) => {
      let JSONresponse = JSON.parse(response);
      //  this.call_subject_with_zone(this.edit_saved_subject, JSONresponse);
    });

  }

  printReport(reportData: any) {
    this._electronService.ipcRenderer.send('patient:onPrint',reportData);
  }
  printMultiReport(reportData: any) {
    this._electronService.ipcRenderer.send('patient:onMultiPrint',reportData);
  }
  //==================================================================
  // ipcRenderer runs outside of angular ZONE
  // so all subjects defined inside ipcRenderer.on are wrapped 
  // inside zone.run() method.
  //==================================================================
  call_subject_with_zone(passed_subject: Subject<any>, subject_data: any) {
    this.zone.run(() => {
      passed_subject.next(subject_data);
    });
  }


}
