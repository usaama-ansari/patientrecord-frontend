import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormGroup, NgForm, FormControl, FormArray } from '@angular/forms';
import { ModifyService } from '../modify.service';
import { SuggesstionsService } from '../../../services/suggesstions.service';
import { Subscription } from 'rxjs/Subscription';
import {
  PatientRecordInterface,
  NewTestInterface,
  InvestigationInterface
} from '../../../models/patient-record.model';
import {
  investigationParameters,
  investigationResults,
  clinicalDiagnosis,
  specimensArray
} from '../../../config/config';


@Component({
  selector: 'app-tests',
  templateUrl: './tests.component.html',
  styleUrls: ['./tests.component.css']
})


export class TestsComponent implements OnInit, OnDestroy {
  @Input() patientToEdit: PatientRecordInterface;//#### patient data passed when edit id clicked
  editForm: FormGroup;
  sugg_rec_sub_subs: Subscription;
  suggObject: { source: string, suggArray: string[] }={ source: '', suggArray: [] };
  testToEdit: NewTestInterface = { //#### particular test data passed when test of patientToEdit is selected
    labNo: null,
    testName: null,
    serumValue: null,
    clinicalDiagnosis: null,
    dateOfCollection: null,
    investigations: null,
    dateOfTesting: null,
    physician: null,
    specimen: null,
  };
  editedTestIndex: number; // INDEX OF TEST BEING EDITED (used to update on backend)
  editedTestId: string;
  clinicalDiagnosis = clinicalDiagnosis;
  specimensArray = specimensArray;
  investigationParameters = investigationParameters;
  investigationResults = investigationResults;
  investigationArray: InvestigationInterface[];// bound via two way binding
  testSelected: boolean = false;// to display the test edit portion on selecting particular test
  selected: number = null;
  disabled: boolean = false;
  constructor(
    private _modifyService: ModifyService,
    private _sugService: SuggesstionsService
  ) { }

  ngOnInit() {
    this.onSelect(0);
    this.sugg_rec_sub_subs = this._sugService.sugg_rec_subject.subscribe(
      (suggesstions: { source: string, suggArray: string[] }) => {
        console.log(this.suggObject)
        this.suggObject = suggesstions;
      })
  }


  ngOnDestroy() {
    this.sugg_rec_sub_subs.unsubscribe();
  }
  getSuggestions(source: string, value: string) {
    console.log(value)
    this._sugService.get_suggesstions(source, value);
  }

  onSelect(testIndex: number) {
    this.editedTestIndex = testIndex;
    this.testToEdit = this.patientToEdit.tests[testIndex];
    this.editedTestId = this.testToEdit['_id'];
    this.investigationArray = this.testToEdit.investigations.slice();
    this.testSelected = true;
    this.selected = testIndex;
    this.init_form();
  }


  isActive(index: number) {
    return this.selected === index;
  }

  //#### initialized when particular test is selected
  init_form() {
    this.editForm = new FormGroup({
      clinicalDiagnosis: new FormControl(this.testToEdit.clinicalDiagnosis),
      dateOfCollection: new FormControl(this.testToEdit.dateOfCollection),
      dateOfTesting: new FormControl(this.testToEdit.dateOfTesting),
      physician: new FormControl(this.testToEdit.physician),
      labNo: new FormControl(this.testToEdit.labNo),
      testName: new FormControl(this.testToEdit.testName),
      serumValue: new FormControl(this.testToEdit.serumValue),
      specimen: new FormControl(this.testToEdit.specimen),
      investigations: new FormArray([])
    });
    //#### 
    this.testToEdit.investigations.forEach((inv: InvestigationInterface) => {
      (<FormArray>this.editForm.get('investigations')).push(new FormControl(inv.result))
    });
  }

  onSave() {
    this.disabled = true;
    //restructure the edited test data
    this.editForm.value.investigations = this.investigationArray;
    this._modifyService.saveEditedTest(this.editForm.value, this.editedTestIndex, this.patientToEdit._id);
  }


}
