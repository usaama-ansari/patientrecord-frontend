import { Component, OnInit, OnDestroy, NgZone, ApplicationRef } from '@angular/core';
import { PatientService } from '../../services/patient.service';
import { ModifyService } from './modify.service';
import { PatientRecordInterface } from '../../models/patient-record.model';
import { Subscription } from 'rxjs/Subscription';
import { SuggesstionsService } from '../../services/suggesstions.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/take';


@Component({
  selector: 'app-modify',
  templateUrl: './modify.component.html',
  styleUrls: ['./modify.component.css']
})
export class ModifyComponent implements OnInit, OnDestroy {
  edit: boolean = false;// to hide or display list of patients and tests component
  edit_saved_subj_subscr: Subscription;
  patientsArray: PatientRecordInterface[];
  patient_list_subject_subscription: Subscription;
  edit_saved_subject_subscription: Subscription;
  patient_del_success_subject_subs: Subscription;
  patientToEdit: PatientRecordInterface = null;
  patientToEditIndex: number = null;
  deleteIndex: number = null;
  suggObject: { source: string, suggArray: string[] } = { source: '', suggArray: [] };
  sugg_rec_sub_subs: Subscription;
  constructor(
    private _patientService: PatientService,
    public _appRef: ApplicationRef,
    private _modifyService: ModifyService,
    private zone: NgZone,
    private _sugService: SuggesstionsService,
  ) { }


  ngOnInit() {

    this.patient_list_subject_subscription = this._patientService.patient_list_subject.subscribe((response) => {
      this.patientsArray = response['msg'];
    });
    this.edit_saved_subject_subscription = this._modifyService.edit_saved_subject.subscribe((response) => {
      if (response['success']) {
        alert('Successfully saved the changes.');
        // update the patientsArray with the new updated patient
        this.update_patients_array(response['msg'].updatedPatient);
        this.goBack();
      }
    });
    this.patient_del_success_subject_subs = this._modifyService.patient_del_success_subject.subscribe((index) => {
      this.patientsArray.splice(index, 1);
    })
    this._patientService.fetchPatients({ search_by: null, value: null });
    this.sugg_rec_sub_subs = this._sugService.sugg_rec_subject.subscribe(
      (suggesstions: { source: string, suggArray: string[] }) => {
        this.suggObject = suggesstions;
      });
  }


  ngOnDestroy() {
    this.patient_list_subject_subscription.unsubscribe();
    this.edit_saved_subject_subscription.unsubscribe();
    this.patient_del_success_subject_subs.unsubscribe();
    this.sugg_rec_sub_subs.unsubscribe();
  }

  //to display test component while binding patientToEdit in its template
  onEditStart(patientToEdit: PatientRecordInterface, patientIndex: number) {
    this.patientToEdit = patientToEdit;
    this.patientToEditIndex = patientIndex;
    this.edit = true;
  }
  searchPatient(search_by: string, value: string) {
    this._patientService.fetchPatients({ search_by, value });
  }
  update_patients_array(updatedPatient: PatientRecordInterface) {
    this.patientsArray[this.patientToEditIndex] = updatedPatient;
  }

  getSuggestions(source: string, value: string) {
    this._sugService.get_suggesstions(source, value);
  }
  deleteRecord() {
    this._modifyService.deleteRecord(this.patientsArray[this.deleteIndex]._id, this.deleteIndex);
  }


  goBack() {
    this.patientToEdit = null;
    this.patientToEditIndex = null;
    this.edit = false;
  }


}
