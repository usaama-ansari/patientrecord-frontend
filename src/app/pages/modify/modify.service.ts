import { Injectable, NgZone } from '@angular/core';
import { ElectronService } from 'ngx-electron';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ModifyService {
  edit_saved_subject: Subject<any> = new Subject();// subscribed in modify.component.ts
  patient_del_success_subject: Subject<any> = new Subject();
  deletedPatientIndex: number;
  constructor(
    private _electronService: ElectronService,
    private zone: NgZone
  ) {
    this.init_ipc();
  }


  saveEditedTest(editedTest: object, editedTestIndex: number, _id: any) {
    var data = { editedTest, editedTestIndex, _id };
    this._electronService.ipcRenderer.send('patient:onTestEdit', data);
  }

  init_ipc() {
    this._electronService.ipcRenderer.on('patient:onTestEditComplete', (event, response) => {
      let JSONresponse = JSON.parse(response);
      this.call_subject_with_zone(this.edit_saved_subject, JSONresponse);
    });

    this._electronService.ipcRenderer.on('patient:deleted', (event, response) => {
      if (response['success']) {
        this.call_subject_with_zone(this.patient_del_success_subject, this.deletedPatientIndex)
        alert('patient record deleted.')
      } else {
        alert('failed to delete record.')
      }
    });

  }

  //==================================================================
  // ipcRenderer runs outside of angular ZONE
  // so all subjects defined inside ipcRenderer.on are wrapped 
  // inside zone.run() method.
  //==================================================================
  call_subject_with_zone(passed_subject: Subject<any>, subject_data: any) {
    this.zone.run(() => {
      passed_subject.next(subject_data);
    });
  }

  deleteRecord(_id: any,index: number) {
    this.deletedPatientIndex = index;
    this._electronService.ipcRenderer.send('patient:delete', _id);
  }

}
