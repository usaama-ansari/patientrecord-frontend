import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ElectronService } from 'ngx-electron';

@Component({
  selector: 'app-new-edit',
  templateUrl: './new-edit.component.html',
  styleUrls: ['./new-edit.component.css']
})
export class NewEditComponent implements OnInit {
  @Input() mode: string;
  nameRegex: string = '^[A-z]+$';

    constructor(
      private _electronService: ElectronService
    ) { }

  ngOnInit() {

  }

  // ON FORM SUBMIT
  onSubmit(form: NgForm) {
    console.log(form.value);
    //reset form
    form.reset();
  }

  // WHEN USER CHOOSE TO SAVE RECORD AND PRINT REPORT RIGHT AWAY  
  onCreateAndPrint(form: NgForm) {
    this._electronService.ipcRenderer.send("report:print", { data: form.value });
  }

  // WHEN USER RESETS THE FORM IS DIRTY
  reset(form: NgForm) {
    if (form.dirty) {
      form.reset();
    }
  }


}
