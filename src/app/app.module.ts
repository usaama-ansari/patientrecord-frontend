import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgxElectronModule } from 'ngx-electron';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { AddComponent } from './pages/add/add.component';
import { ModifyComponent } from './pages/modify/modify.component';
import { ViewComponent } from './pages/view/view.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './header/header.component';
import { NewEditComponent } from './components/new-edit/new-edit.component';
import { NewPatientComponent } from './pages/add/new-patient/new-patient.component';
import { TestComponent } from './components/test/test.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './ng-material-module/material.module';
import { SpecimenComponent } from './pages/add/new-patient/specimen/specimen.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { TestsComponent } from './pages/modify/tests/tests.component';
import { PrintComponent } from './pages/view/print/print.component';
import { FullViewComponent } from './pages/view/full-view/full-view.component';
//SERIVES
import { PatientService } from './services/patient.service';
import { SuggesstionsService } from './services/suggesstions.service';
import { ModifyService } from './pages/modify/modify.service';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AddComponent,
    ModifyComponent,
    ViewComponent,
    HeaderComponent,
    NewEditComponent,
    NewPatientComponent,
    TestComponent,
    TestsComponent,
    PrintComponent,
    FullViewComponent,
    SpecimenComponent,
    SettingsComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    NgxElectronModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  providers: [PatientService, ModifyService, SuggesstionsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
