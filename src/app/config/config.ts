

export const clinicalDiagnosis = [
  'Viral Hepatitis',
  'Dengue Fever'
];
export const investigationParameters = [
 'HbsAg',
 'AntiHCV',
 'HCV(IgG)',
 'HSV-1(IgM)',
 'HSV-2(IgM)',
 'Dengue IgM', 
 'Dengue IgG', 
 'Dengue NS-1 Antigen',
 'Rotavirus IgM',
 'Hepatitis A-IgM',
 'Hepatitis E-IgM',
 'Rotavirus Antigen',
 'JE-IgM',
 'Chikunguniya-IgM',
 'Enterovirus-IgM',
 'H3N2',
 'Measles-IgM',
 'Panovirus-IgM',
 'Mumps-IgM',
 'Rubella-IgM',
 'Varicella Zoaster (VZV)-IgM',
 'Epstein-Barrvirus (EBV)-IgM',
]
// array to iterate select options  
export const investigationResults = [
  'Result pending',
  'Reactive',
  'Non-Reactive',
  'Borderline Positive',
  'Equivocal'
]
//Array to be populated by user

export const specimensArray: string[] = [
  'blood serum',
  'stool',
  'cerebrospinal fluid',
  'nasopharyngeal aspirate',
  'nasopharyngeal swab',
  'sputum'
]

