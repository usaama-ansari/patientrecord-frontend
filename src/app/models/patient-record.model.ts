
export interface PatientRecordInterface {
    _id: any,
    patientId: string,
    name: string,
    age: string,
    gender: string,
    altId: string,
    labNo: string,
    tests: NewTestInterface[]
}

export interface TestInterface {
    specimen: string,
    physician: string,
    clinicalDiagnosis: string,
    investigations: InvestigationInterface[],
    dateOfCollection: string,
    dateOfTesting: string,
}

export interface NewTestInterface {
    labNo: string,
    specimen: string,
    physician: string,
    testName: string,
    serumValue: string,
    clinicalDiagnosis: string,
    investigations: InvestigationInterface[],
    dateOfCollection: string,
    dateOfTesting: string,
}
export interface InvestigationInterface {      
    parameter: string,
    result: string
}

export class Patient {
    constructor(public patientRecord: PatientRecordInterface) {

    }
}