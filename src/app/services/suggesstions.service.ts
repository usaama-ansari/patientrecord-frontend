import { Injectable, NgZone } from '@angular/core';
import { ElectronService } from 'ngx-electron';
import { Subject } from 'rxjs/Subject';
import { switchMap } from 'rxjs/operators';
@Injectable()
export class SuggesstionsService {

  sugg_rec_subject = new Subject();

  constructor(
    private _electronService: ElectronService,
    private zone: NgZone
  ) {
    this.init_ipc();
  }

  get_suggesstions(source: string, value: string) {
    this._electronService.ipcRenderer.send('suggesstions:request', { source, value });
  }


  //===================================================================================================
  //===================================================================================================
  //#######################     INITIALIZE IPC      ###################################################
  //
  init_ipc() {
    // FETCH PATIENT ID IPC
    this._electronService.ipcRenderer.on('suggesstions:response', (event, response) => {
      this.call_subject_with_zone(this.sugg_rec_subject, response);
    });

  }

  call_subject_with_zone(passed_subject: Subject<any>, subject_data: any) {
    this.zone.run(() => {
      passed_subject.next(subject_data);
    })
  }


}