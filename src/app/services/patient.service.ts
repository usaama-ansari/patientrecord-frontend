import { Injectable, NgZone } from '@angular/core';
import { ElectronService } from 'ngx-electron';
import { PatientRecordInterface } from '../models/patient-record.model';
import { Subject } from 'rxjs/Subject';
@Injectable()
export class PatientService {
  patient_list_subject: Subject<any> = new Subject();// subscribed in view.component.ts, modify.component.ts
  patient_id_subject: Subject<any> = new Subject();  // subscribed in new-patient.component.ts
  current_menu_subject = new Subject<string>();

  constructor(
    private _electronService: ElectronService,
    private zone: NgZone
  ) {
    this.init_ipc();
  }
  //===================================================================================================
  //===================================================================================================
  //#######################     INITIALIZE IPC      ###################################################
  //
  init_ipc() {
    // FETCH PATIENT ID IPC
    this._electronService.ipcRenderer.on('patient:receiveId', (event, response) => {
      let JSONresponse = JSON.parse(response);
      this.call_subject_with_zone(this.patient_id_subject, JSONresponse);
    });

    // FETCH PATIENT LIST IPC
    this._electronService.ipcRenderer.on('patient:receivedList', (event, response) => {
      var JSONresponse = JSON.parse(response);
      //send data with subject to view.component.ts, modify.component.ts
      this.call_subject_with_zone(this.patient_list_subject, JSONresponse)
    });
    //ON BACKUP SUCCESSFULLY DONE
    this._electronService.ipcRenderer.on('patient:backUpDone',(event)=>{
      alert('Backup successfully saved on desktop.')
    })

  }
  //======================================================================================================
  //=======================================================================================================


  //==================================================================
  // ipcRenderer runs outside of angular ZONE
  // so all subjects defined inside ipcRenderer.on are wrapped 
  // inside zone.run() method.
  //==================================================================
  call_subject_with_zone(passed_subject: Subject<any>, subject_data: any) {
    this.zone.run(() => {
      passed_subject.next(subject_data);
    })
  }

  //########   ADD NEW PATIENT VIA FORM 
  addNewPatient(newPatientRecord: PatientRecordInterface) {
    // send Patient Record to electorn process to store in db
    this._electronService.ipcRenderer.send('patient:create', newPatientRecord);
  }

  //########   FETCH LIST OF ALL PATIENTS FROM DATABASE
  fetchPatients(query) {
    this._electronService.ipcRenderer.send('patient:fetchList', query);
  }


  backUpRecords(timeRange: any) {
    this._electronService.ipcRenderer.send('patient:backUp', timeRange);
  }


  setId(id: number) {
    this._electronService.ipcRenderer.send('patient:setId', id);
  }

  edit() {

  }

  //######### GET LAST SAVED ID(and show it by increamenting 1)  OF PATIENT TO SHOW WHILE CREATING NEW PATIENT
  getId() {
    this._electronService.ipcRenderer.send('patient:fetchId');
  }





}
